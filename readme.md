# Демо сценария CI/CD Kubernetes 

В данном репозитории описана инструкция для поднятия демонстрационного DevOps стенда в VK Cloud. В результате должен получиться кластер kubernetes с crud-приложением, хранящим данные в отдельном инстансе postgres. Приложение разбито на микросервисы и доступ к нему осуществляется через ingress-контроллер. При пуше нового коммита, после прохождения всех этапов пайплайна, новая версия приложения развернётся в кластере kubernetes.

Пререквизиты:
- Кластер [kubernetes](https://mcs.mail.ru/docs/ru/base/k8s/quickstart): мастер 2-4, 3 воркер-ноды 2-4, с включенным автоскейлингом и установленными аддоном ingress-nginx.
- Сингл-инстанс сервиса [баз данных](https://mcs.mail.ru/docs/ru/dbs/dbaas/quick-start) Postgresql 14 версии в той же сети, что кластер kubernetes. 
- Личный репозиторий в Gitlab.

# (Опционально) Поднятие инфраструктуры через Terraform

Можно создать кластер с бд через графический интерфейс, или использовать подход infrastructure as a code. Воспользуемся инструментом terraform, для которого у VK cloud имеется свой [провайдер](https://github.com/vk-cs/terraform-provider-vkcs). Также имеется раздел [документации](https://mcs.mail.ru/docs/ru/manage/tools-for-using-services/terraform) на портале облака. Терраформ можно скачать с [зеркала vk cloud](https://hashicorp-releases.mcs.mail.ru/terraform)

Указываем данные для доступа к апи облака через terraform, при помощи команды:

```bash
source <имя rc файла из личного кабинета vk cloud в разделе "Доступ через API">
```

Конфигурационные файлы для поднятия инфраструктуры находятся в директории terraform. Файлы из этого репозитория следует скопировать в свой собственный и вести работу в нём.

(Опционально) Можно указать удалённое хранилище в виде s3 для tfstate. Для этого создайте бакет s3 в vkcloud и укажите его имя  и добавьте этот блок кода в файл terraform/provider.tf

```bash
terraform {
  backend "s3" {
    bucket   = <имя бакета s3>
    key      = "terraform.tfstate"
    endpoint = "https://hb.ru-msk.vkcs.cloud/"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}
```

Или можно не указывать и хранить стейт локально.

Указываем env переменную с паролем, который будет установлен инстансу базы данных:

```bash
export TF_VAR_db_password=<пароль бд>
```

Переходим в папку с файлами terraform:

```bash
cd terraform
```

Инициализируем terraform:

```bash
terraform init
```

Применяем конфигурационные файлы:

```bash
terraform apply
```

Вывод покажет список аддонов, и статус их установки (installed: true/false)

```shell
cluster-addons = {
  "addons" = tolist([
    {
      "id" = "7c2fb2f7-0364-4440-b831-96bb7d3ce9c7"
      "installed" = false
      "name" = "ingress-nginx"
      "version" = "4.1.4"
    },
    {
      "id" = "abe1f963-f0bc-40d5-851f-e6cf754d1858"
      "installed" = false
      "name" = "kiali"
      "version" = "1.59.1"
    },
    {
      "id" = "bb33c252-cdd1-4876-9b44-9c60f495f6d5"
      "installed" = false
      "name" = "kube-prometheus-stack"
      "version" = "36.2.0"
    },
    {
      "id" = "c95284f9-e533-4e13-911b-c8f97fc3af48"
      "installed" = false
      "name" = "docker-registry"
      "version" = "2.2.2"
    },
    {
      "id" = "d3bcc9cc-3864-4047-8409-cd677b0e62ba"
      "installed" = false
      "name" = "istio"
      "version" = "1.16.4"
    },
    {
      "id" = "fd384c52-f821-4a7c-ba64-ddb0c0545871"
      "installed" = false
      "name" = "jaeger"
      "version" = "0.71.4"
    },
  ])
  "cluster_id" = "2af576ed-d432-4f87-a7c0-632ca15e5c22"
  "id" = "2919766474"
  "region" = "RegionOne"
}
```

В случае надобности установки дополнительных аддонов, нужно взять данные из списка по нужному аддону, и указать их в data типа vkcs_kubernetes_addon и сослаться на него в resource того же типа.

```hcl
data "vkcs_kubernetes_addon" "ingress-nginx" {
  cluster_id = vkcs_kubernetes_cluster.devops-stand-cluster.id
  name       = "ingress-nginx"
  version    = "4.1.4"
}

resource "vkcs_kubernetes_addon" "ingress-nginx" {
  cluster_id           = vkcs_kubernetes_cluster.devops-stand-cluster.id
  addon_id             = data.vkcs_kubernetes_addon.ingress-nginx.id
  namespace            = "ingress-nginx"
  configuration_values = data.vkcs_kubernetes_addon.ingress-nginx.configuration_values

  depends_on = [
    vkcs_kubernetes_node_group.workload-2-4
  ]
}
```

# Подготовка репозитория и кластера для работы с ним

Создайте новый private репозиторий в публичном гитлабе.

![создание проекта](media/project_creation.png)

Клонируйте репозиторий и перейдите в директорию с ним. Скопируйте в ваш репозиторий содержимое этого репозитория.

Для подключения к kubernetes вам понадобится утилита keystone authentication и kubectl.

Подключитесь к кластеру kubernetes. Подробнее в [документации](https://mcs.mail.ru/docs/ru/base/k8s/connect/kubectl)

Установите раннеры гитлаба в кластер kubernetes.

Добавим репозиторий с helm-чартом ([установить helm](https://helm.sh/docs/intro/install/)):

```bash
helm repo add gitlab https://charts.gitlab.io
```

Если репозиторий уже добавлен, можно проверить его на обновления:

```bash
helm repo update gitlab
```

Cоздадим secret с токеном раннера. В web-интерфейсе gitlab нужно получить токен.

Создадим раннер:

![создание раннера](media/create_runner_pt1.png)

Выбираем тип раннера linux. Здесь не так важен тип, как просто токен:

![выбор типа раннера](media/create_runner_pt2.png)

Копируем runner-token, он понадобиться нам для установки gitlab runner'ов через helm:

![токен раннера](media/create_runner_pt3.png)

Создадим секрет, с полученным runner token'ом:

```bash
cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: dev-gitlab-runner
  namespace: gitlab
type: Opaque
stringData:
  runner-registration-token: ""
  # тут подставляем полученный в WEB интерфейсе токен
  runner-token: "glrt-1KJ_ky-66jzYyrpvxM_L"
EOF
```

Деплоим раннеры, не забываем про gitlab-config-values.yml

```bash
helm install gitlab-runner gitlab/gitlab-runner -f gitlab-config-values.yml -n gitlab
```
# Деплой собранного приложения

Создадим сервис-аккаунт, который будет использован gitlab'ом для деплоя приложений в кластер:

```bash
kubectl create sa deploy -n vk-cloud-project
```

и rolebinding

```bash
kubectl create rolebinding deploy \
  -n vk-cloud-project \
  --clusterrole edit \
  --serviceaccount vk-cloud-project:deploy
```

Создадим токен под этот сервис аккаунт, токен будет использован гитлабом:

```bash
kubectl apply -n vk-cloud-project -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: deploy
  annotations:
    kubernetes.io/service-account.name: deploy
type: kubernetes.io/service-account-token
EOF
```

Получим значение токена сервис аккаунта:

```bash
kubectl get secrets -n vk-cloud-project deploy -o jsonpath='{.data.token}'
```

В разделе переменных гитлаба создадим переменную K8S_CI_TOKEN со значением токена полученным ранее:

![ci token option](media/create_var_pt1.png)

![ci token creation](media/create_var_pt2.png)

Далее, нужно создать токен, который будет использоваться kubernetes для извлечения образов из реджистри гитлаба.

Создаём pull token в web-интерфейсе:

![create deploy token option](media/create_deploy_token_pt1.png)

![create deploy token](media/create_deploy_token_pt2.png)

Сохраним значения, полученные при выводе:

![deploy token vals](media/create_deploy_token_pt3.png)

Создадим namespace для деплоя приложений:

Создадим секрет, который будет использоваться для доступа к реджистри гитлаба:

```bash
kubectl create secret docker-registry vkcloud-kubernetes-project-image-pull \
  --docker-server registry.gitlab.com \
  --docker-email 'admin@mycompany.com' \
  --docker-username '<первая строка>' \
  --docker-password '<вторая строка>' \
  --namespace vk-cloud-project
```

# Подготовка базы данных для работы с приложением

Создадим namespace для деплоя приложений:

```bash
kubectl create namespace vk-cloud-project
```

Создадим секрет, который под будет использовать для доступа к базе данных. Данные используем, которые ранее указывались при создании инстанса бд postgres.

```bash
kubectl create secret generic user-app -n vk-cloud-project \
  --from-literal=DB_NAME=testdb \
  --from-literal=DB_USER=user1 \
  --from-literal=DB_PASSWORD=pLP90s6Wi1G$ \
  --from-literal=DB_HOST=192.168.199.14 \
  --from-literal=DB_PORT=5432
```

# Запуск пайплайна и проверка работоспособности приложения

После внесения всех изменений, можно сделать git push. В результате должен выполниться пайплайн и задеплоиться приложения:

![deploy app](media/deploy_app.png)

Для просмотра внешнего айпи ингресса, куда можно слать запросы, нужно выполнить команду:

```bash
kubectl get svc -n ingress-nginx
```

```shell
NAME                                 TYPE           CLUSTER-IP       EXTERNAL-IP              PORT(S)                      AGE
ingress-nginx-controller             LoadBalancer   10.254.228.206   109.120.180.206.nip.io   80:32320/TCP,443:31447/TCP   6h30m
ingress-nginx-controller-admission   ClusterIP      10.254.235.104   <none>                   443/TCP                      6h30m
ingress-nginx-controller-metrics     ClusterIP      10.254.233.242   <none>                   9913/TCP                     6h30m
ingress-nginx-default-backend        ClusterIP      10.254.132.85    <none>                   80/TCP                       6h30m
```

Смотрим на TYPE = LoadBalancer и значение EXTERANL-IP. Можно обращаться по dns имени или отбросить ".nip.io" и использовать внешний ip.

# Проверка работоспособности приложения

**Сервис get users**

Получение списка пользователей:

```bash
curl <внешний ip ingress>/info/users
```

вывод:

```shell
[{"id":1,"name":"Maxim","location":"Moscow","age":21},{"id":2,"name":"Dima","location":"Moscow","age":22}]
```

Получение пользователя по id:

```bash
curl <внешний ip ingress>/info/user/{id}
```

вывод:

```shell
{"id":1,"name":"Maxim","location":"Moscow","age":21}
```

**Сервис add user**

Добавить пользователя:

```bash
curl -X POST -H "Content-Type: application/json" -d '{
    "name": "Dima",
    "location": "Moscow",
    "age": 22
}' http://<внешний_ip_ingress>/user-storage/user
```

вывод:

```shell
{"id":2,"message":"User created successfully"}
```

# Проверка выкатки новых версий

При пуше нового коммита, будет происходить новый деплоймент и его версию можно проверить вызовом:

для сервиса add_user:
```bash
curl http://<внешний_ip_ingress>/user-storage/version
```

для сервиса get_users:
```bash
curl http://<внешний_ip_ingress>/info/version
```

Для теста изменений можно изменить вывод сообщения у метода /info/hello, для этого в файле **app/internal/middleware/handlers.go** отредактируйте переменную **helloMessage**:

```go
func Hello(w http.ResponseWriter, r *http.Request) {
	helloMessage := "hello message" // здесь можно изменить сообщение

	res := response {
		Message: helloMessage,
	}

	json.NewEncoder(w).Encode(res)
}
```

# Тестирование автомасштабирования

Мы настроили автомасштабирование в файле **kube/get-users-hpa.yml** для сервиса get-users и для нод в настройках кластера.

Проверить его можно, подав нагрузку на сервис.

Применим файл **get-users-load-generator.yml**. Подставим, полученный ранее внешний ip ingress в файл. Также мы можем изменять количество реплик для изменения нагрузки.

```bash
kubectl apply -f get-users-load-generator.yml
```

Статус hpa можно посмотреть командой:

```bash
kubectl get hpa
```

В случае недостаточности ресурсов кластера для новых реплик, поды будут в статусе pending, пока autoscaler не создаст новые ноды.

# Удаление ресурсов

После удаления источника нагрузки, поды, созданные hpa, и ноды, созданные autoscaler, будут удалены.

Удалить кластер и инстанс базы данных можно через личный кабинет, или в случае с terraform, командой 

```bash
tarraform destroy
```
