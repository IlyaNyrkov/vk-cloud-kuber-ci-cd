package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"mcs-kubernetes-project/internal/middleware"
	"net/http"
	"os"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/info/version", middleware.Version).Methods("GET")
	router.HandleFunc("/info/user/{id}", middleware.GetUser).Methods("GET", "OPTIONS")
	router.HandleFunc("/info/users", middleware.GetUsers).Methods("GET", "OPTIONS")
	router.HandleFunc("/info/version", middleware.Version).Methods("GET")
	router.HandleFunc("/info/hello", middleware.Hello).Methods("GET")

	log.Printf("Starting server on the port %s...\n", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), router))
}
