package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"mcs-kubernetes-project/internal/middleware"
	"net/http"
	"os"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/user-storage/user", middleware.CreateUser).Methods("POST", "OPTIONS")
	router.HandleFunc("/user-storage/user/{id}", middleware.UpdateUser).Methods("PUT", "OPTIONS")
	router.HandleFunc("/user-storage/user/{id}", middleware.DeleteUser).Methods("DELETE", "OPTIONS")
	router.HandleFunc("/user-storage/version", middleware.Version).Methods("GET")
	router.HandleFunc("/info/hello", middleware.Hello).Methods("GET")

	log.Printf("Starting server on the port %s...\n", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), router))
}
