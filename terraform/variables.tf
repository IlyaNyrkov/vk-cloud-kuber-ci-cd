# ---- KUBERNETES -----

variable "k8s_master_count" {
  type = number
  default = 1
}

variable "k8s_master_volume_size" {
  type = number
  default = 50
}

variable "etcd_volume_size" {
  type = number
  default = 20
}

variable "kubelet_log_level" {
  type = number
  default = 8
}

variable "k8s_monitoring_enabled" {
  type = bool
  default = true
}

variable "insecure_registries" {
  type = list(string)
  default = ["1.2.3.4"]
}

variable "k8s_floating_ip_enabled" {
  type = bool
  default = true
}

# 
variable "workload-2-4-node-count" {
  type = number
  default = 2
}

variable "workload-2-4-max-node-count" {
  type = number
  default = 4
}

variable "workload-2-4-node-volume-type" {
  type = string
  default = "ceph-ssd"
}

# ---- DATABASE -----

variable "db_charset" {
  type = string
  default = "utf8"
}

variable "db_username" {
  type = string
  default = "user1"
}

variable "db_password" {
  type = string
  sensitive = true
}

variable "db_type" {
  type = string
  default = "postgresql"
}

variable "db_version" {
  type = string
  default = "14"
}

variable "db_volume_type" {
  type = string
  default = "ceph-ssd"
}

variable "db_volume_size" {
  type = number
  default = 20
}

variable "db_volume_max_size" {
  type = number
  default = 1000
}

variable "db_disk_autoexpand" {
  type = bool
  default = true
}

# ---- COMMON -----

variable "az" {
  type = string
  default = "MS1"
}

variable "dns_name_servers" {
  type = list(string)
  default = ["8.8.8.8", "8.8.4.4"]
}

variable "ci-cd-subnet-cidr" {
  type = string
  default = "192.168.199.0/24"
}

variable "sdn" {
  type = string
  default = "sprut"
}