# ---- COMMON -----

# Виртульная сеть, объединяющая виртуальные подсети
resource "vkcs_networking_network" "ci-cd-net" {
  name           = "ci-cd-net"
  admin_state_up = true
  sdn = var.sdn
}

# Виртуальная подсеть
resource "vkcs_networking_subnet" "ci-cd-subnet" {
  name            = "ci-cd-subnet"
  network_id      = vkcs_networking_network.ci-cd-net.id
  cidr            = var.ci-cd-subnet-cidr
  dns_nameservers = var.dns_name_servers
  sdn = var.sdn

  depends_on = [ 
    vkcs_networking_network.ci-cd-net 
    ]
}

# Виртуальный роутер для подключения подсетей и доступа базы данных и kubernetes в интернет
resource "vkcs_networking_router" "ci-cd-router" {
  name                = "ci-cd-router"
  admin_state_up      = true
  external_network_id = data.vkcs_networking_network.extnet.id
  sdn = var.sdn

  depends_on = [ 
    vkcs_networking_network.ci-cd-net,
    vkcs_networking_subnet.ci-cd-subnet
    ]
}

# Подключение подсети к роутеру
resource "vkcs_networking_router_interface" "ci-cd-router-int" {
  router_id = vkcs_networking_router.ci-cd-router.id
  subnet_id = vkcs_networking_subnet.ci-cd-subnet.id
  sdn = var.sdn
}

# ---- KUBERNETES -----

resource "vkcs_kubernetes_cluster" "devops-stand-cluster" {
  name                = "devops-stand-cluster"
  cluster_template_id = data.vkcs_kubernetes_clustertemplate.ct.id
  # Тип (флейвор) ноды мастера
  master_flavor       = data.vkcs_compute_flavor.k8s_node.id
  # Количество мастеров в кластере 
  master_count        = var.k8s_master_count

  # Сетевое подключение
  network_id          = vkcs_networking_network.ci-cd-net.id
  subnet_id           = vkcs_networking_subnet.ci-cd-subnet.id
  # Будет ли назначен внешний ip балансеровщику, используемому для доступа к кластеру
  floating_ip_enabled = var.k8s_floating_ip_enabled
  # Зона доступности в которой будет создан кластер (ms1, gz1, qaz)
  availability_zone   = var.az
  # Список адресов реджистри, у которых не будет проверятся tls-сертификат
  insecure_registries = var.insecure_registries

  # ingress_floating_ip = 

  labels = {
    # Тип диска мастер ноды
    cluster_node_volume_type = var.workload-2-4-node-volume-type
    # Размер диска ноды мастера
    master_volume_size = var.k8s_master_volume_size
    # Мониторинг нод сервисом vk cloud
    cloud_monitoring = var.k8s_monitoring_enabled
    # Размер отдельного диска ноды мастера, выделенного etcd
    etcd_volume_size = var.etcd_volume_size
    # Степень логирования процессов kubelet на нодах
    kube_log_level = var.kubelet_log_level
  }

  depends_on = [
    vkcs_networking_router_interface.ci-cd-router-int
  ]
}


# Описание воркер ноды кластера kubernetes
resource "vkcs_kubernetes_node_group" "workload-2-4" {
    name = "workload-2-4"
    cluster_id = vkcs_kubernetes_cluster.devops-stand-cluster.id
    flavor_id = data.vkcs_compute_flavor.k8s_node.id
    node_count = var.workload-2-4-node-count
    min_nodes = var.workload-2-4-node-count
    max_nodes = var.workload-2-4-max-node-count

    labels {
        key = "disktype"
        value = var.workload-2-4-node-volume-type
    }

    depends_on = [
      vkcs_kubernetes_cluster.devops-stand-cluster
     ]
}

# Датасорс со списком всех шаблонов кластера 
data "vkcs_kubernetes_clustertemplates" "templates" {}

# Вывод списка шаблонов кластера после применения plan/apply
output "cluster-templates" {
  description = "List of cluster templates"
  value = data.vkcs_kubernetes_clustertemplates.templates
}

# ---- KUBERNETES ADDONS--------

# Датасорс со статусом всех аддонов кластера установлен/не установлен
data "vkcs_kubernetes_addons" "cluster-addons" {
  cluster_id = vkcs_kubernetes_cluster.devops-stand-cluster.id
}

# Вывод статуса аддонов на кластере
output "cluster-addons" {
  description = "List of ci-cd cluster addons"
  value       = data.vkcs_kubernetes_addons.cluster-addons
}

# Данные аддона ingress nginx controller для kubernetes 
data "vkcs_kubernetes_addon" "ingress-nginx" {
  cluster_id = vkcs_kubernetes_cluster.devops-stand-cluster.id
  name       = "ingress-nginx"
  version    = "4.7.1"
}

# Установка аддона ingress nginx controller в создаваемый кластер
resource "vkcs_kubernetes_addon" "ingress-nginx" {
  cluster_id           = vkcs_kubernetes_cluster.devops-stand-cluster.id
  addon_id             = data.vkcs_kubernetes_addon.ingress-nginx.id
  namespace            = "ingress-nginx"
  configuration_values = data.vkcs_kubernetes_addon.ingress-nginx.configuration_values

  depends_on = [
    vkcs_kubernetes_node_group.workload-2-4
  ]
}




# ---- DATABASE -----

# Описание инстанса бд
resource "vkcs_db_instance" "db-instance" {
  name        = "devops-stand-db-instance"

  availability_zone = var.az

  # Здесь задаётся тип платформенного сервиса баз данных (postgresql/mysql и т.д.)
  datastore {
    type    = var.db_type
    version = var.db_version
  }

  flavor_id   = data.vkcs_compute_flavor.db.id
  
  # Размер диска бд
  size        = var.db_volume_size
  # Тип диска бд
  volume_type = var.db_volume_type

  # Параметры автомасштабирования диска
  disk_autoexpand {
    # Включена опция или нет
    autoexpand    = var.db_disk_autoexpand
    max_disk_size = var.db_volume_max_size
  }

  # Сетевое подключение
  network {
    uuid = vkcs_networking_network.ci-cd-net.id
  }

  depends_on = [
   vkcs_networking_router_interface.ci-cd-router-int
  ]
}

# Описание объекта базы данных
resource "vkcs_db_database" "testdb" {
  name        = "testdb"
  dbms_id     = vkcs_db_instance.db-instance.id
  charset     = var.db_charset
}

# Описание пользователя базы данных
resource "vkcs_db_user" "db-user" {
  name        = var.db_username
  # Для паролей и других sensitive данных лучше использовать 
  # хранилища секретов или задавать значение через переменную окружения
  password    = var.db_password
  dbms_id     = vkcs_db_instance.db-instance.id
  databases   = [vkcs_db_database.testdb.name]
}
