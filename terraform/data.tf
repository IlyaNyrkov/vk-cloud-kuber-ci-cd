# ---- KUBERNETES -----

data "vkcs_compute_flavor" "k8s_node" {
  name = "Standard-2-4-50"
}

data "vkcs_kubernetes_clustertemplate" "ct" {
  version = "1.27"
}

# ---- DATABASE -----

data "vkcs_compute_flavor" "db" {
  name = "Standard-2-8-50"
}

# ---- COMMON -----

data "vkcs_networking_network" "extnet" {
  name = "internet"
  sdn = "sprut"
}